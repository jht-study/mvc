//
//  File.swift
//  MVC
//
//  Created by Administrador on 04/12/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

import UIKit

class GreetingController: UIViewController {
    
    //*************************************************
    // MARK: - Outlets
    //*************************************************
    
    @IBOutlet private var showGreetingButton : UIButton!
    @IBOutlet private var greetingLabel : UILabel!
    
    //*************************************************
    // MARK: - Private Properties
    //*************************************************
    
    private var person: Person = Person(firstName: "Jeferson", lastName: "Takumi")
    
    //*************************************************
    // MARK: - Lifecycle
    //*************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showGreetingButton.addTarget(self, action: #selector(didTapButton(_ :)), for: .touchUpInside)
    }
    
    //*************************************************
    // MARK: - Actions
    //*************************************************
    
    @objc func didTapButton(_ button: UIButton) {
        let greeting = "Hello \(person.firstName) \(person.lastName)"
        self.greetingLabel.text = greeting;
    }
}
