//
//  Info.swift
//  MVC
//
//  Created by Administrador on 22/11/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

import Foundation

struct Header {
    
    let title: String
    let detail: String
    
    func toString() -> String {
        return title + ": " + detail
    }
}
