//
//  Person.swift
//  MVC
//
//  Created by Administrador on 04/12/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

struct Person {
    let firstName: String
    let lastName: String
}
